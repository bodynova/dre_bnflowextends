<?php

namespace Bender\dre_BnFlowExtends\Application\Controller\Admin;

//use Bender\dre_BnFlowExtends\Application\Models\Cronjob;
//use Bender\dre_BnFlowExtends\Core\Module\Module;
use Bender\dre_BnFlowExtends\Application\Models\dre_Debug;
use OxidEsales\Eshop\Application\Controller\Admin\AdminController;
use OxidEsales\Eshop\Core\Module\ModuleList;

class dre_Admin extends AdminController
{
    /**
     * Current class template name.
     *
     * @var string
     */
    protected $_sThisTemplate = 'dre_admin.tpl';


    /**
     * dre_Admin constructor.
     */
    public function __construct()
    {
        parent::__construct();

        // Neu einlesen aller vorhandenen Cronjobs
        $sModulesDir = $this->getConfig()->getModulesDir();

        /** @var ModuleList $oModuleList */
        $oModuleList = oxNew( ModuleList::class );


        /** @var Module $oModule */
        foreach( $oModuleList->getModulesFromDir( $sModulesDir ) as $oModule ) {
            if( ! count( $oModule->getCronjobs() ) ) continue;

            foreach( $oModule->getCronjobs() as $sCronjobId => $aCronjobConfig ){
                if( ! isset( $aCronjobConfig[ 'crontab' ] ) ) continue;

                // Prüfen, ob der Cronjob bereits in der Datenbank ist
                /** @var Cronjob $oCronjob */
                $oCronjob = oxNew( dre_Debug::class );
                if( ! $oCronjob->loadByCronjobId( $sCronjobId, $oModule->getId() ) ){
                    $oCronjob->assign([
                        'oxstatus' => 'active',
                        'oxcronjobid' => $sCronjobId,
                        'oxcrontab' => $aCronjobConfig[ 'crontab' ],
                        'oxmoduleid' => $oModule->getId(),
                    ]);
                    $oCronjob->save();
                }
            }
        }
    }
}