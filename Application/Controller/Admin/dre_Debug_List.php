<?php

namespace Bender\dre_BnFlowExtends\Application\Controller\Admin;

use Bender\dre_BnFlowExtends\Application\Models\dre_Debug;
use OxidEsales\Eshop\Application\Controller\Admin\AdminListController;

class dre_Debug_List extends AdminListController
{
    /**
     * Current class template name.
     *
     * @var string
     */
    protected $_sThisTemplate = 'dre_debug_list.tpl';

    /**
     * Name of chosen object class (default null).
     *
     * @var string
     */
    protected $_sListClass = dre_Debug::class;

    /**
     * Type of list.
     *
     * @var string
     */
    protected $_sListType = \Bender\dre_BnFlowExtends\Application\Models\dre_Debug_List::class;
}