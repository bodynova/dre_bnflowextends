<?php
namespace Bender\dre_BnFlowExtends\Application\Models;

use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Registry;

class dre_BnFlowExtends_Helper extends dre_BnFlowExtends_Helper_parent
{

    /**
     * gibt ein ConfigObjekt zurück
     * @return \OxidEsales\Eshop\Core\Config
     */
    public static function BnGetConfig() {
        return Registry::getConfig();
    }

    public static function BnGetSession(){
        return Registry::getSession();
    }

    public static function BnGetLang(){
        return Registry::getLang();
    }

    public static function BnGetUtils(){
        return Registry::getUtils();
    }

    public static function BnGetUtilsObject(){
        return Registry::getUtilsObject();
    }

    public static function BnGetInputValidator(){
        return Registry::getInputValidator();
    }

    public static function BnGetPictureHandler(){
        return Registry::getPictureHandler();
    }

    public static function BnGetRequest(){
        return Registry::getRequest();
    }

    public static function BnGetSeoEncoder(){
        return Registry::getSeoEncoder();
    }

    public static function BnGetSeoDecoder(){
        return Registry::getSeoDecoder();
    }

    public static function BnGetUtilsCount(){
        return Registry::getUtilsCount();
    }

    public static function BnGetUtilsDate(){
        return Registry::getUtilsDate();
    }

    public static function BnGetUtilsFile(){
        return Registry::getUtilsFile();
    }


    public static function BnGetUtilsPic() {
        return Registry::getUtilsPic();
    }

    public static function BnGetUtilsServer() {
        return Registry::getUtilsServer();
    }

    public static function BnGetUtilsString() {
        return Registry::getUtilsString();
    }

    public static function BnGetUtilsUrl() {
        return Registry::getUtilsUrl();
    }

    public static function BnGetUtilsXml() {
        return Registry::getUtilsXml();
    }

    public static function BnGetUtilsView() {
        return Registry::getUtilsView();
    }

    public static function BnGetControllerClassNameResolver() {
        return Registry::getControllerClassNameResolver();
    }

    /**
     * @return \Psr\Log\LoggerInterface
     * public function emergency($message, array $context = array());
     * public function alert($message, array $context = array());
     * public function critical($message, array $context = array());
     * public function error($message, array $context = array());
     * public function warning($message, array $context = array());
     * public function notice($message, array $context = array());
     * public function info($message, array $context = array());
     * public function debug($message, array $context = array());
     * public function log($level, $message, array $context = array());
     */
    public static function BnGetLogger() {
        return Registry::getLogger();
    }

    public static function BnGetKeys() {
        return Registry::getKeys();
    }

    /*
    public static function BnG {
        return Registry::;
    }
    */

}
