<?php

namespace Bender\dre_BnFlowExtends\Application\Model;


use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Model\BaseModel;

/**
 * Class Log
 *
 * @property Field drelog__oxid char(32)
 * @property Field drelog__oxmodulid char(32)
 * @property Field drelog__oxstarttime decimal(16,6)
 * @property Field drelog__oxendtime decimal(16,6)
 * @property Field drelog__oxstate enum('running', 'finished', 'aborted')
 * @property Field drelog__oxexception text
 * @property Field drelog__oxtimestamp timestamp
 */
class dre_Log extends BaseModel
{
    /**
     * Current class name.
     *
     * @var string
     */
    protected $_sClassName = self::class;

    /**
     * Class constructor, initiates parent constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('dre_log');
    }


    /**
     * Lädt den letzten Log eines bestimmten Cronjobs
     *
     * @param $sCronjobId
     * @return bool
     */
    public function loadLastLogForModul( $sModulId )
    {
        /*
        //getting at least one field before lazy loading the object
        $this->_addField('oxid', 0);
        $sSelect = $this->buildSelectString(array($this->getViewName() . '.oxmodulid' => $sModulId ));

        // Order
        $sSelect .= 'ORDER BY ' . $this->getViewName() . '.oxstarttime DESC LIMIT 1';

        $this->_isLoaded = $this->assignRecord($sSelect);

        return $this->_isLoaded;
        */
        return false;
    }
}