<?php

namespace Bender\dre_BnFlowExtends\Application\Models;

use Bender\dre_BnFlowExtends\Application\Model\dre_Log;
use OxidEsales\Eshop\Core\Model\ListModel;

class dre_Log_List extends ListModel
{
    /**
     * Class constructor, initiates parent constructor (parent::oxList()).
     */
    public function __construct()
    {
        parent::__construct(dre_Log::class);
    }


    /**
     * Lädt aktive Cronjobs in die Liste
     *
     * @return $this
     */
    public function loadActiveCronjobs()
    {
        /*
        $listObject = $this->getBaseObject();
        $fieldList = $listObject->getSelectFields();

        $query = "select $fieldList from " . $listObject->getViewName();
        $query .= " where oxstatus = 'active'";
        $this->selectString($query);

        return $this;
        */
    }
}