<?php

namespace Bender\dre_BnFlowExtends\Core;

use OxidEsales\Eshop\Core\FileCache;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\ConfigFile;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;

class Events
{
    /**
     * Führt bei Modul-aktivierung einige SQL-Befehle aus
     *
     * @access  public
     */
    public static function onActivate()
    {
        // Datenbank-Objekt auslesen
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

        $aSql = [];
        /*
        $aSql[] = <<<SQL
CREATE TABLE `dredebug` (
  `OXID` char(32) COLLATE utf8_general_ci NOT NULL,
  `OXSTATUS` enum('active','paused','aborted') COLLATE utf8_general_ci DEFAULT NULL,
  `OXCRONJOBID` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `OXCRONTAB` varchar(128) COLLATE utf8_general_ci DEFAULT NULL,
  `OXTIMESTAMP` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `OXMODULEID` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`OXID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT=''
SQL;
        */

        $aSql[] = <<<SQL
CREATE TABLE `drelog` (
  `OXID` char(32) COLLATE utf8_general_ci NOT NULL,
  `OXMODULID` char(32) COLLATE utf8_general_ci NOT NULL,
  `OXSTARTTIME` decimal(16,6) DEFAULT NULL,
  `OXENDTIME` decimal(16,6) DEFAULT NULL,
  `OXSTATE` enum('running','finished','aborted') COLLATE utf8_general_ci NOT NULL,
  `OXLOG` text COLLATE utf8_general_ci,
  `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OXID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Log-Table for Bodynova Modules'
SQL;

        foreach ($aSql as $sSql) {
            // MySQL-Fehler abfangen
            try {
                $blResult = $oDb->execute($sSql);
            } catch (DatabaseErrorException $e) {
                // Ausser es sind keine "alread exists" fehler...
                if (!preg_match('/(already exists|Duplicate column name)/i', $e->getMessage())) {
                    throw $e;
                }
            }
        }

        self::clearCache();
    }

    /**
     * Bei Modul-Deaktivierung
     */
    public static function onDeactivate()
    {
        self::clearCache();
    }

    /**
     * Löscht den TMP-Ordner sowie den Smarty-Ordner
     */
    protected static function clearCache()
    {
        /** @var FileCache $fileCache */
        $fileCache = oxNew(FileCache::class);
        $fileCache::clearCache();

        /** Smarty leeren */
        $tempDirectory = Registry::get(ConfigFile::class)->getVar("sCompileDir");
        $mask = $tempDirectory . "/smarty/*.php";
        $files = glob($mask);
        if (is_array($files)) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    @unlink($file);
                }
            }
        }
    }
}