<?php

namespace Bender\dre_BnFlowExtends\Core\Module;

class dre_Module extends Module_parent
{
    /**
     * Gibt eine Liste der DebugModule zurück
     *
     * @return array
     */
    public function getDebugModules()
    {
        return isset($this->_aModule['debug']) ? $this->_aModule['debug'] : [];
    }
}