<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id'            => 'dre_bnflowextends',
    'title'         => '<img src="../modules/dre_bnflowextends/out/img/favicon.ico" title="Bodynova Klassenmodul Theme">odynova Template erweiterungen',
    'description' => [
        'de' => 'Template erweiterungen',
        'en' => 'Theme extendings'
    ],
    'thumbnail' => 'out/img/logo_bodynova.png',
    'version'     => '1.5.0',
    'author'      => 'André Bender',
    'url'         => 'https://bodynova.de',
    'email'         => 'support@bodynova.de',
    'controllers' => array(
    ),
    'extend' => array(

    ),
    'blocks'        => array(
	),
    'templates' => array(

    ),  
    'events'    => array(
    ),
);
